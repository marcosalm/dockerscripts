# dockerscripts
Scripts importantes


Para instalar o docker:

Baixe o projeto;



```bash
git clone git@github.com:krotondigital/dockerscripts.git scripts
```
Dê permissao de execução no script

```bash
chmod +x scripts/install_docker.sh
```

Instale o docker com:

```bash
./scripts/install_docker.sh
```

Vá ser feliz com sua nova instalação!
